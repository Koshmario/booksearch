import bookSearch from './components/book-search/bookSearch.vue'
import bookContent from './components/book-content/bookContent.vue'
import eventBus from './components/eventBus.js'

export default {
  name: 'app',
  data: function () {
    return {
      authorization: false,
      favoritesBooks: [],
      access_token: '',
      isFavorite: false
    }
  },
  components: {
    bookSearch,
    bookContent
  },
  created () {
    eventBus.$on('changeIsFavorite', (val) => {
      this.isFavorite = val
    })
    eventBus.$on('setFavoriteBook', (dataBook) => {
      this.favoritesBooks.push(dataBook)
      // console.log(dataBook.description)
    })
    eventBus.$on('removeFavoriteBook', (dataBook) => {
      let fv = this.favoritesBooks
      for (let i = 0; fv.length > i; i++) {
        if (fv[i].idBook === dataBook.idBook) {
          fv.splice(i, 1)
          break
        }
      }
    })
  },
  methods: {

    getFavoritesBooks: function () {
      let requestString =
        'https://www.googleapis.com/books/v1/mylibrary/bookshelves/0/volumes?' +
        'maxResults=100' +
        '&access_token=' +
        this.access_token

      let xhr = new XMLHttpRequest()
      xhr.onreadystatechange = function (vm) {
        if (this.readyState === XMLHttpRequest.DONE) {
          if (this.status === 200) {
            let obj = JSON.parse(this.response)
            let items = obj.items
            if (items) {
              for (let i = 0; items.length > i; i++) {
                let titleValue = obj.items[i].volumeInfo.title
                if (titleValue.length > 150) {
                  titleValue = titleValue.substr(0, 150) + '...'
                }
                let descriptionValueFull

                let descriptionValue = obj.items[i].volumeInfo.description

                let dateValue = new Date(obj.items[i].volumeInfo.publishedDate).getFullYear()

                let imageLinkValue = obj.items[i].volumeInfo.imageLinks

                let authors = obj.items[i].volumeInfo.authors

                let authorsValue = ''

                if (authors) {
                  for (let authorsNum = 0; authors.length > authorsNum; authorsNum++) {
                    if (authorsNum !== 0) {
                      authorsValue = authorsValue + ', ' + authors[authorsNum]
                    } else {
                      authorsValue = authorsValue + authors[authorsNum]
                    }
                  }
                } else {
                  authorsValue = 'Автор не указан'
                }

                !imageLinkValue ? imageLinkValue = 0 : imageLinkValue = imageLinkValue.thumbnail

                if (descriptionValue) {
                  if (descriptionValue.length > 200) {
                    descriptionValueFull = descriptionValue.substr(0, 1200) + '...'
                    descriptionValue = descriptionValue.substr(0, 200) + '...'
                  } else {
                    descriptionValueFull = descriptionValue
                  }
                } else {
                  descriptionValue = 'Описание отсутсвует.'
                  descriptionValueFull = 'Описание отсутсвует.'
                }
                vm.favoritesBooks.push(
                  {
                    title: titleValue,
                    authors: authorsValue,
                    date: dateValue,
                    description: descriptionValue,
                    descriptionFull: descriptionValueFull,
                    imageLink: imageLinkValue,
                    rating: obj.items[i].volumeInfo.averageRating,
                    number: i,
                    idBook: obj.items[i].id
                  })
              }
            }
          }
        }
      }.bind(xhr, this)

      xhr.open('GET', requestString)
      xhr.send()
    },

    getAuthorizationValue: function () {
      if (localStorage.getItem('oauth2-params') !== null) {
        let params = JSON.parse(localStorage.getItem('oauth2-params'))
        if (params && params['access_token']) {
          let xhr = new XMLHttpRequest()

          let dataValue = this
          xhr.open('GET',
            'https://www.googleapis.com/drive/v3/about?fields=user&' +
            'access_token=' + params['access_token'])
          xhr.onreadystatechange = function () {
            if (xhr.readyState === 4 && xhr.status === 200) {
              console.log(xhr.response)
            } else if (xhr.readyState === 4 && xhr.status === 401) {
              // Token invalid, so prompt for user permission.
              dataValue.authorization = false
            } else if (xhr.readyState === 4 && xhr.status === 403) {
              dataValue.authorization = true
            }
          }
          xhr.send(null)
        }
      }
    }
  },
  beforeMount () {
    this.access_token = JSON.parse(localStorage.getItem('oauth2-params')).access_token
    this.getAuthorizationValue()
    this.getFavoritesBooks()
  },
  beforeCreate () {
    let fragmentString = location.hash.substring(1)
    // Parse query string to see if page request is coming from OAuth 2.0 server.
    let params = {}
    let regex = /([^&=]+)=([^&]*)/g
    let m
    while (m = regex.exec(fragmentString)) {
      params[decodeURIComponent(m[1])] = decodeURIComponent(m[2])
    }
    if (Object.keys(params).length > 0) {
      localStorage.setItem('oauth2-params', JSON.stringify(params))
    }
  }
}
