import vSelect from 'vue-select'
import eventBus from '../eventBus.js'
import searchResult from './search-result/searchResult.vue'

export default {
  name: 'bookSearch',
  data: () => {
    return {
      searchRequest: '',
      searchBooks: [],
      autoComplete: [],
      statusRequest: ''
    }
  },

  props: {
    authorization: Boolean,
    favoritesBooks: Array,
    access_token: String,
    isFavorite: Boolean
  },
  components: {
    vSelect,
    searchResult
  },

  watch: {
    searchRequest: function (val) {
      this.searchResponse(val)
    }
  },

  methods: {

    changeIsFavorite: function (val) {
      eventBus.$emit('changeIsFavorite', val)
    },

    searchResponse: function (requestString) {
      if (this.searchRequest === requestString && requestString !== '') {
        requestString =
          'https://www.googleapis.com/books/v1/volumes?q=intitle:' + requestString +
          '&maxResults=15' +
          '&fields=items(' +
          'id,' +
          'volumeInfo/title,' +
          'volumeInfo/authors,' +
          'volumeInfo/publishedDate,' +
          'volumeInfo/description,' +
          'volumeInfo/imageLinks/thumbnail,' +
          'volumeInfo/averageRating)' +
          '&access_token=' +
          this.access_token

        let xhr = new XMLHttpRequest()
        xhr.onreadystatechange = function (vm) {
          vm.statusRequest = 'loading'

          if (this.readyState === XMLHttpRequest.DONE) {
            if (this.status === 200) {
              let obj = JSON.parse(this.response)

              if (obj.items) {
                vm.searchBooks = []
                vm.autoComplete = []
                vm.statusRequest = 'success'
                for (let i = 0; obj.items.length > i; i++) {
                  let titleValue = obj.items[i].volumeInfo.title
                  if (titleValue.length > 150) {
                    titleValue = titleValue.substr(0, 150) + '...'
                  }
                  vm.autoComplete.push(titleValue)

                  let descriptionValueFull

                  let descriptionValue = obj.items[i].volumeInfo.description

                  let dateValue = new Date(obj.items[i].volumeInfo.publishedDate).getFullYear()

                  let imageLinkValue = obj.items[i].volumeInfo.imageLinks

                  let authors = obj.items[i].volumeInfo.authors

                  let authorsValue = ''

                  if (authors) {
                    for (let authorsNum = 0; authors.length > authorsNum; authorsNum++) {
                      if (authorsNum !== 0) {
                        authorsValue = authorsValue + ', ' + authors[authorsNum]
                      } else {
                        authorsValue = authorsValue + authors[authorsNum]
                      }
                    }
                  } else {
                    authorsValue = 'Автор не указан'
                  }

                  !imageLinkValue ? imageLinkValue = 0 : imageLinkValue = imageLinkValue.thumbnail

                  if (descriptionValue) {
                    if (descriptionValue.length > 200) {
                      descriptionValueFull = descriptionValue.substr(0, 1200) + '...'
                      descriptionValue = descriptionValue.substr(0, 200) + '...'
                    } else {
                      descriptionValueFull = descriptionValue
                    }
                  } else {
                    descriptionValue = 'Описание отсутсвует.'
                    descriptionValueFull = 'Описание отсутсвует.'
                  }
                  vm.searchBooks.push(
                    {
                      title: titleValue,
                      authors: authorsValue,
                      date: dateValue,
                      description: descriptionValue,
                      descriptionFull: descriptionValueFull,
                      imageLink: imageLinkValue,
                      rating: obj.items[i].volumeInfo.averageRating,
                      idBook: obj.items[i].id
                    })
                }
              } else {
                vm.searchBooks = []
                vm.autoComplete = []
                vm.statusRequest = 'no-result'
              }
            } else if (this.status === 401) {
              vm.statusRequest = 'token-invalid'
            } else {
              vm.searchBooks = []
              vm.statusRequest = 'limit'
            }
          }
        }.bind(xhr, this)

        xhr.open('GET', requestString)
        xhr.send()
      } else {
        this.searchRequest = requestString
      }
    },

    reset: function () {
      this.searchRequest = ''
      this.searchBooks = []
      this.autoComplete = []
    },

    oauth2SignIn: () => {
      // Google's OAuth 2.0 endpoint for requesting an access token
      let YOUR_CLIENT_ID = '240577313438-b4votiphu90p8odk20hrurqd6cs38jtm.apps.googleusercontent.com'
      let YOUR_REDIRECT_URI = 'http://localhost:8080/'
      let oauth2Endpoint = 'https://accounts.google.com/o/oauth2/v2/auth'

      // Create element to open OAuth 2.0 endpoint in new window.
      let form = document.createElement('form')
      form.setAttribute('method', 'GET') // Send as a GET request.
      form.setAttribute('action', oauth2Endpoint)

      // Parameters to pass to OAuth 2.0 endpoint.
      let params = {
        'client_id': YOUR_CLIENT_ID,
        'redirect_uri': YOUR_REDIRECT_URI,
        'scope': 'https://www.googleapis.com/auth/books',
        'state': 'try_sample_request',
        'include_granted_scopes': 'true',
        'response_type': 'token'
      }

      // Add form parameters as hidden input values.
      for (let p in params) {
        let input = document.createElement('input')
        input.setAttribute('type', 'hidden')
        input.setAttribute('name', p)
        input.setAttribute('value', params[p])
        form.appendChild(input)
      }

      // Add form to page and submit it to open the OAuth 2.0 endpoint.
      document.body.appendChild(form)
      form.submit()
    }
  }
}
