import eventBus from '../../../components/eventBus.js'

export default {
  name: 'searchResult',
  data: function () {
    return {}
  },
  props: {
    booksShow: Array,
    statusRequest: String,
    favoritesBooks: Array,
    isFavorite: Boolean
  },
  methods: {

    showBook: function (idBook) {
      let bookIndex = this.booksShow.findIndex(x => x.idBook === idBook)

      let book = this.booksShow[bookIndex]

      let rating = book.rating

      let favoriteText = 'В избранное'

      let favoritesBooks = this.favoritesBooks

      for (let i = 0; favoritesBooks.length > i; i++) {
        if (favoritesBooks[i].idBook === book.idBook) {
          this.isFavorite ? favoriteText = 'Удалить' : favoriteText = 'Перейти'
        }
      }

      if (!rating) {
        rating = 0
      }

      let bookData = {
        title: book.title,
        authors: book.authors,
        date: book.date,
        descriptionFull: book.descriptionFull,
        description: book.description,
        imageLink: book.imageLink,
        rating: rating,
        idBook: book.idBook,
        favoriteText: favoriteText
      }
      eventBus.$emit('bookDataChange', bookData)
    }
  }
}
