import eventBus from '../eventBus.js'

export default {
  name: 'bookContent',
  data: () => {
    return {
      bookData: {},
      contentBackground: true
    }
  },
  props: {
    access_token: String,
    isFavorite: Boolean
  },
  methods: {

    changeIsFavorite: function (val) {
      eventBus.$emit('changeIsFavorite', val)
      this.bookData.favoriteText = 'Удалить'
    },

    addBookInFavorite: function (idBook) {
      let requestString =
        'https://www.googleapis.com/books/v1/mylibrary/bookshelves/0/addVolume?' +
        'volumeId=' + idBook +
        '&key=AIzaSyD7n81jYNc-Q-sGZyC2FvchsNoUuevfZI8' +
        '&access_token=' +
        this.access_token

      let xhr = new XMLHttpRequest()
      xhr.onreadystatechange = function (vm) {
        vm.bookData.favoriteText = 'Добавляем...'

        if (this.readyState === XMLHttpRequest.DONE) {
          if (this.status === 204) {
            vm.isFavorite ? vm.bookData.favoriteText = 'Удалить' : vm.bookData.favoriteText = 'Перейти'
          } else {
            vm.bookData.favoriteText = 'Ошибка добавления'
          }
        }
      }.bind(xhr, this)

      xhr.open('POST', requestString)
      xhr.setRequestHeader('Content-Type', 'application/json')
      xhr.send()
      eventBus.$emit('setFavoriteBook', this.bookData)
    },

    removeBookInFavorite: function (idBook) {
      let requestString =
        'https://www.googleapis.com/books/v1/mylibrary/bookshelves/0/removeVolume?' +
        'volumeId=' + idBook +
        '&key=AIzaSyD7n81jYNc-Q-sGZyC2FvchsNoUuevfZI8' +
        '&access_token=' +
        this.access_token

      let xhr = new XMLHttpRequest()
      xhr.onreadystatechange = function (vm) {
        vm.bookData.favoriteText = 'Удаляем...'

        if (this.readyState === XMLHttpRequest.DONE) {
          if (this.status === 204) {
            vm.bookData.favoriteText = 'В избранное'
          } else {
            vm.bookData.favoriteText = 'Ошибка удаления'
          }
        }
      }.bind(xhr, this)

      xhr.open('POST', requestString)
      xhr.setRequestHeader('Content-Type', 'application/json')
      xhr.send()
      eventBus.$emit('removeFavoriteBook', this.bookData)
    }

  },
  created () {
    eventBus.$on('bookDataChange', (bookData) => {
      this.bookData = bookData
      if (this.contentBackground) {
        this.contentBackground = false
      }
    })
    eventBus.$on('changeIsFavorite', () => {
      if (this.bookData.favoriteText === 'Удалить') {
        this.bookData.favoriteText = 'Перейти'
      }
    })
  }
}
